package main

import (
	"fmt"
)

type Dinero int

// Declaramos el método String para el tipo Dinero
// d Dinero: es el receiver (recibidor)
func (d Dinero) String() string {
	return fmt.Sprintf("$%d", d)
}

func main() {
	var sueldo Dinero
	sueldo = 25000
	fmt.Println("Sueldo:", sueldo) // Llama al método String() del tipo Dinero!!!

	aumento := 10000 // es de tipo int
	// sueldo += aumento //mismatched types Dinero and int
	sueldo += Dinero(aumento) // Es necesaria la conversión de manera explicita

	fmt.Println("Sueldo + aumento:", sueldo)
}