package main

import (
	"fmt"
	"os"
	"time"
)

func leerEntrada(out chan<- []byte) {
	for {
		datos := make([]byte, 1024)
		// os.Stdin.Read: Lee la entrada de datos desde la consola
		n,_ := os.Stdin.Read(datos)
		if n > 0 {
			out <- datos
		}
	}
}

func main() {	
	// After: Devuelve un objeto channel de tipo Time 
	// luego de que pasa el tiempo que le hemos pasado como parámetro
	// Luego de 20 segundos mandame un mensaje por done.
	// Es decir la ejecución estará activa como mucho 20 segundos
	done := time.After(20 * time.Second)
	eco := make(chan []byte)
	go leerEntrada(eco)
	// Va a estar constantemente escaneando los channels
	for {
		// Select es parecido al switch
		// diferencia: evalua cuando a través de un canal viene el dato
		select {
			// Si vienen datos a través del channel eco
			case datos := <-eco:
				// os.Stdout.Write: imprimo los datos por consola
				os.Stdout.Write(datos)
			// Si vienen datos a través del channel done
			case <-done:
				fmt.Println("Se termino el tiempo")
				// Pasando el parametro 0, para decir que el programa se ejecutó sin problemas.
				os.Exit(0)
			}
	}
}