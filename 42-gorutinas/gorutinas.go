package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)
// gorutina: Es un hilo ligero manejado por el runtime de go
// Las computadoras tienen cada vez más nucleos, pero esos no nucleos no estan avanzando de capacidad
// Ejemplo: No se ven nucleos de 10 giga, pero si nucleos de 3 giga, nucleos de 3.algo giga;
// aunque sí muchos nucleos en el procesador. GO fue diseñado para que los programas hechos en este lenguaje
// aprovechen a plenitud una máquina con 12 nucleos, por ejemplo. ES POR ESTO QUE GO ES TAN EFICIENTE
// Ejemplo -> Con java: Requerimientos para ejecutar de manera optima: 12 nucleos y 16 gb de ram, con GO 2
// nucleos y quiza 2 o 4 GB de ram
// Se pueden ejecutar hilos en Java, pero se ejecutan con una biblioteca, que fue creada luego para Java pueda hacer frente a las nuevas tecnoligas
// Go fue creado para las nuevas tecnologias. Resuelve las gorutinas por medio del core de GO.

// Paralelismo: Dos tareas o más tareas en dos o más nucleos. EL control lo tiene el SO.
// Concurrencia: Simulación del procesador de ejecutar de manera paralela más de una tarea en el mismo núcleo
// Gorutina: En un hilo se ejecutan varias gorutinas (concurrencia de GO!!) y el control sobre las mismas lo posee GO (IMPORTANTE!!).

// waitGroup: es una herramienta para trabajar con gorutinas
// wg es utilizado para indicarle al programa que debe esperar que finalicen las gorutinas
var wg sync.WaitGroup

func imprimirCantidad(etiqueta string) {
	// Llamamos a la función Done() de wg para indicarle que la gorutina termina
	defer wg.Done()
	// Espera aleatoria
	for cantidad := 1; cantidad <= 10; cantidad++ {
		// Int63n: devuelve un numero random de 0 a 1000
		sleep := rand.Int63n(1000)
		// * time.Millisecond: para que el resultado se convierta en milliseconds
		// Existe time.Minute, time.Microsecond, etc.
		time.Sleep(time.Duration(sleep) * time.Millisecond)
		fmt.Printf("Cantidad %d de %s\n", cantidad, etiqueta)
	}
}

func main() {
/* 	fmt.Println("*********** EJECUCIÓN SECUENCIAL ***********")
	fmt.Println("Iniciamos...")
	imprimirCantidad("A")
	imprimirCantidad("B")
	fmt.Println("Esperando que finalicen...")
	fmt.Println("Terminando el programa")
 */
/* 	fmt.Println("*********** EJECUCIÓN CONCURRENTE usando GORUTINAS ***********")
	// Añadimos 2 a wg para que espere que finalicen 2 gorutinas
	// Voy a ejecutar dos gorutinas. Es la cantidad de gorutinas que el programa debe esperar para finalizar
	wg.Add(2)
	fmt.Println("Iniciamos...")
	// Lanzamos la gorutina con la etiqueta A
	go imprimirCantidad("A")
	// Lanzamos la gorutina con la etiqueta B
	go imprimirCantidad("B")
	fmt.Println("Esperando que finalicen...")
	// Cuando llegue aqui espera a que terminen las gorutinas añadidas al grupo
	wg.Wait()
	fmt.Println("Terminando el programa")
 */
	fmt.Println("*********** EJECUCIÓN CONCURRENTE usando GORUTINAS - 2 caso ***********")
	fmt.Println("---> Aquí solo esperamos que finalicen dos gorutinas, si queda una sin terminar lo ignora")
	// Añadimos 2 a wg para que espere que finalicen 2 gorutinas
	// Voy a ejecutar dos gorutinas. Es la cantidad de gorutinas que el programa debe esperar para finalizar
	wg.Add(2)
	fmt.Println("Iniciamos...")
	// Lanzamos la gorutina con la etiqueta A
	go imprimirCantidad("A")
	// Lanzamos la gorutina con la etiqueta B
	go imprimirCantidad("B")
	// Lanzamos la gorutina con la etiqueta B
	go imprimirCantidad("C")
	fmt.Println("Esperando que finalicen...")
	// Cuando llegue aqui espera a que terminen las gorutinas añadidas al grupo
	// Es decir, que traba el programa hasta que finalicen la cantidad de gorutinas establecidas en ws.Add
	wg.Wait()
	fmt.Println("Terminando el programa")

}