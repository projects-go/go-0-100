package main

import (
	"fmt"
	"strings"
)

// Función NOMBRADA
func mapper(r rune) rune { // El código UTF-8 de cada caracter en go es rune
	return r + 1 // AL valor ITF-8 de cada caracter le sumamos uo
}

/*
func Map(mapping func(rune) rune, s string) string
Map returns a copy of the string s with all its characters modified according
to the mapping function. If mapping returns a negative value, the character is
dropped from the string with no replacement.
*/
func main() {
	cadena := "123456789"
	// func(r rune) rune {...es una función ANONIMA. Podría haber usado la NOMBRADA AQUÍ
	cadena = strings.Map(func(r rune) rune { // El código UTF-8 de cada caracter en go es rune
		return r + 1 // AL valor ITF-8 de cada caracter le sumamos uo
	}, cadena)

	fmt.Println(cadena)
}