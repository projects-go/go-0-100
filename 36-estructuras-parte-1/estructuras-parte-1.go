package main

import (
	"fmt"
)

type Persona struct {
	Nombre string
	Edad int
}

func Older(p1, p2 Persona) (Persona, int) {
	if p1.Edad > p2.Edad {
		return p1, p1.Edad - p2.Edad
	}
	return p2, p2.Edad - p1.Edad
}

func main() {
	var p Persona
	p.Nombre = "Arnaldo"
	p.Edad = 33
	fmt.Println("Estructura p de tipo Persona:", p)
	fmt.Println("Nombre p:", p.Nombre)
	fmt.Println("Edad p:", p.Edad)

	p2 := Persona{Nombre: "Raúl", Edad:25}
	fmt.Println("Nombre p2:", p2.Nombre)
	fmt.Println("Edad p2:", p2.Edad)

	p3 := Persona{"Miguel", 44}
	fmt.Println("Nombre p3:", p3.Nombre)
	fmt.Println("Edad p3:", p3.Edad)

	tom := Persona{"Tom", 60}
	bob := Persona{"Bob", 25}
	paul := Persona{"Paul", 43}

	tbOlder, tbDiff := Older(tom, bob)
	tpOlder, tpDiff := Older(tom, paul)
	bpOlder, bpDiff := Older(bob, paul)

	fmt.Printf("Entre %s y %s es mayor %s por %d edad años\n", tom.Nombre, bob.Nombre, tbOlder.Nombre, tbDiff)
	fmt.Printf("Entre %s y %s es mayor %s por %d edad años\n", tom.Nombre, paul.Nombre, tpOlder.Nombre, tpDiff)
	fmt.Printf("Entre %s y %s es mayor %s por %d edad años\n", bob.Nombre, paul.Nombre, bpOlder.Nombre, bpDiff)

}