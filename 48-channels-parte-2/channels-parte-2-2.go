package main

import (
	"fmt"
	"time"
)

// En este ejemplo se sincronizan 3 gorutinas: las dos anónimas y la gorutina main

func main() {
	numero := make(chan int)
	cuadrado := make(chan int)

	// Generamos los números
	go func() {
		for x := 0; x < 5; x++ {
			numero <- x
		}
		// Para cerrar un canal
		// Cuando lo cerramos, lo que hacemos es indicar que no se van a enviar más mensajes a través desde el channel
		close(numero)
	}()

	// Lo enviamos al cuadrado
	go func() {
		for {
			// Cuando el channel esta cerrado lo que recibe es el zero value del channel
			// Para saber si channel esta cerrado se usa ok
			x, ok := <- numero
			if !ok {
				break
			}
			cuadrado <- x * x	
		}
		close(cuadrado)
	}()

	// Desde la gorutina main tomamos el valor del canal cuadrado para imprimirlo
	for {
		x, ok := <-cuadrado
		if !ok {
			break
		}
		fmt.Println(x)
		time.Sleep(1 * time.Second)
	}
}