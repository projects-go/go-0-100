package main

import "fmt"

func sumar(numeros ...int) int {
	resultado := 0
	for _, numero := range numeros {
		resultado += numero
	}
	return resultado
}

func print(cadena string, cadenas ...string) {
	for _, c := range cadenas {
		cadena += " " + c
	}
	fmt.Println(cadena)
}

func main() {
	fmt.Println(sumar())
	fmt.Println(sumar(1, 2, 3))
	fmt.Println(sumar(1, 2, 3, 4, 5, 6))

	numeros := []int{7,8,9}
	fmt.Println(sumar(numeros...))

	print("Hola", "mundo", "con", "go")
}