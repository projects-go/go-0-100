package main

import (
	"fmt"
	"os" // Para tener acceso a funciones del sistema operativo. Es independiente del SO
)

func primera() {
	fmt.Println("Primera")
}

func segunda() {
	fmt.Println("Segunda")
}

/*
Util para:
	Cerrar archivos
*/
// Los defer son apilados y ejecutados con LIFO: Primero en Entrar Último en Salir
func main() {
	defer primera() // defer para que la ejecución se ejecute luego de que termine todo el bloque de la función main
	// segunda()
	defer segunda() 
	fmt.Println("*********** LECTURA DE ARCHIVO ***********")
	// Abrimos un archivo
	f, err := os.Open("texto.txt")
	// Verificamos que no haya ocurrido ningún error
	if err != nil {
		panic(err)
	}	
	// Luego de comprobar que fue abierto se establece que debe cerrarse antes de finalizar la ejecución de la función	
	// Lectura limpia del código
	defer f.Close() 
	data := make([]byte, 1207) // Creamos un slice que va a contener lo que vamos a leer del archivo
	cant, err := f.Read(data)
	// Verificamos que no haya ocurrido un error
	if err != nil {
		panic(err)
	}
	// %q para que en vez de que imprima el codigo UTF-8 imprima las letras
	fmt.Printf("Cantidad de byte leidos: %d\nTexto leido: \n%q \nError: %v", cant, data, err)

	fmt.Println("*********** ITERACION CON DEFER ***********")
	for i:=0; i <= 5; i++ {
		defer fmt.Println(i)
	}
}