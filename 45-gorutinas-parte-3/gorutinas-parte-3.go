package main

// Se crea un servidor para permitir a diferentes clientes conectarse a el.
// Sirve la hora.
// No es un servidor Web, sino que va a estar a traves del protocolo TCP.

/*
Para ejecutar abrir dos terminales: 
1ra terminal) go run gorutinas-parte-3.go
2da terminal) nc localhost 8080

Command linux nc
nc - arbitrary TCP and UDP connections and listens
The nc (or netcat) utility is used for just about anything under the sun involving TCP or UDP. 
It can open TCP connections, send UDP packets, listen on arbitrary TCP and UDP ports, 
do port scanning, and deal with both IPv4 and IPv6. Unlike telnet(1), nc scripts nicely, 
and separates error messages onto standard error instead of sending them to standard output, 
as telnet(1) does with some.
*/

import (
	"io"
	"log"
	"net" // permite utilizar el paquete http, el cual sirve para crear servidores con el protocolo TCP, UDP o servidores web (ver esto)
	"time"
)

func manejarCon(c net.Conn) {
	defer c.Close() // Para que antes de terminar la funcion se cierre la conexion
	for {
		// func WriteString(w Writer, s string) (n int, err error)
		// type Writer interface {
		//	 Write(p []byte) (n int, err error)
		// }
		// c (de tipo net.Conn) deberia implementar la interface Write
		// type Conn
		// Conn is a generic stream-oriented network connection.
		// type Conn interface {
		// ...
		// Write(b []byte) (n int, err error)
		//...
		// Es una interface Conn, no un tipo. REVEER ESTO!!!!!!!!!
		_, err := io.WriteString(c, time.Now().Format("15:04:05\n\r")) // Ignoramos la cantidad de bytes que escribio (_)
		if err != nil {
			return // Ejemplo, si el cliente se desconecta
		}
		time.Sleep(1 * time.Second) // Espera un segundo para volver a escribir
	}
}

func main() {
	listener, err := net.Listen("tcp", "localhost:8080") //protocolo (tcp) y la direccion (localhost:8000(puerto))
	if err != nil {
		log.Fatal(err) // No tiene sentido que el programa siga. Este error se da a la hora de crear el servidor.
	}
	for {
		conn, err := listener.Accept() // Para el programa aquí a la espera de conexion
		if err != nil {
			log.Print(err) // Este error es de la conexion entrante.
			continue // Para que intente nuevamente conectarse
		}
		// Caso 1: manejarCon(conn)
		// abrir mas de un terminal y ejecutar nc localhost 8080
		// 1 solo cliente recibe la hora el otro queda en espera
		// Si cierro el cliente que esta recibiendo la hora, el que se encontraba en espera empieza a recibirla
		// Caso 2: go manejarConn(conn)
		// abrir mas de un terminal y ejecutar nc localhost 8080
		// Los dos clientes reciben la hora
		manejarCon(conn) 
	}
}