package main

import "fmt"

func print(cadena string) {
	fmt.Println(cadena)
}

func print2(cadena string) {
	fmt.Println(cadena)
}

func print3(cadena1, cadena2 string) {
	fmt.Println(cadena1 + cadena2)
}

func print4(fprint func(string), cadena string) { //func(string) es el tipo
	fprint(cadena)
}

// Para ejemplificar el CLOSURE
// El retorno es una funcion que no recibe parametros y devuelve un int
func incrementar() func() int { // FUNCION QUE CREA EL ENTORNO
	i := 0  					// ELEMENTO PRIVADO
	return func() (r int) { 	//CLOSURE
		r = i
		i+= 2
		return
	}
}

func incremento() {
	i := 0
	i++
	fmt.Println(i)
}

func main() {
	cadena := "Hola mundo"

	fmt.Println("********* VARIABLE MISMO TIPO QUE FUNCION *********")
	imprimir := print //La función print se le asigna a imprimir
	imprimir(cadena)
	
	imprimir2 := func() {
		fmt.Println(cadena)
	}
	imprimir2()

	imprimir = print2 // Tiene la misma firma que print
	imprimir("Hola mundo 2")

	// imprimir = print3 // No tiene la misma firma que print

	fmt.Println("********* FIRMAS *********")
	fmt.Printf("TIPO de imprimir: %T\n", imprimir)
	fmt.Printf("Firma (TIPO) para función print: %T\n", print)
	fmt.Printf("Firma (TIPO) para función print2: %T\n", print2)
	fmt.Printf("Firma (TIPO) para función print3: %T\n", print3)

	fmt.Println("********* FUNCION COMO PARAMETRO *********")
	print4(print, "Hola Mundo desde Print4")

	fmt.Println("********* FUNCIONES COMPARABLES CON NIL *********")
	var fb func()
	if fb == nil {
		fmt.Println("fb es igual a nil")
	}
	fb = imprimir2
	if fb == nil {
		fmt.Println("fb es igual a nil")
	} else {
		fmt.Println("fb es distinto a nil")
	}
	/*
		var fb2 func()
		if fb == fb2 { // func can only be compared to nil
			fmt.Println("fb es igual a nil")
		} 
	*/
	
	fmt.Println("********* CLOSURE *********")
	inc := incrementar()
	fmt.Println("Valor de i:", inc())
	fmt.Println("Valor de i:", inc())
	fmt.Println("Valor de i:", inc())
	fmt.Println("Valor de i:", inc())

	fmt.Println("********* NO SE PRODUCE EL CLOSURE *********")	
	incremento()
	incremento()
	incremento()
	incremento()
}