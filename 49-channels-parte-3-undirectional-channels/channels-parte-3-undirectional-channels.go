package main

import (
	"fmt"
	"time"
)
//TEMA: UNDIRECTIONAL CHANNELS

// En este ejemplo se sincronizan 3 gorutinas: las dos anónimas y la gorutina main

func generarNumeros(out chan<- int) {
	for x := 0; x < 5 ; x++ {
		out <- x
	}
	// En este caso es necesario avisar a otra gorutina que se cerro el channel
	// Este close no es lo mismo que cuando se cierra por ejemplo un archivo
	// Cuando un channel no esta alcanzable por ninguna gorutina el garbage collector se encarga 
	// de administrar esto
	// Lo puede cerrar porque es de salida
	close(out)
}

func elevarAlCuadrado(in <-chan int, out chan<- int) {
	for x:= range in {
		out <- x * x	
	}
	close(out)
}

func imprimir(in <-chan int) {
	for x := range in {
		fmt.Println(x)
		time.Sleep(1 * time.Second)
		// Errores notificados en tiempo de compilación gracias a la especificación de unidirectional channels
		// in <- 25 //Mensaje de error: invalid operation: in <- 25 (send to receive-only type <-chan int)
		// close(in) // No se puede cerrar un channel de entrada!. Mensaje de error: invalid operation: close(in) (cannot close receive-only channel)
	}
}

func main() {
	numero := make(chan int)
	cuadrado := make(chan int)

	// Generamos los números. 
	// Implicitamente los tranforma de salida, debido a como lo recibe generarNumeros(out chan<- int). 
	// Luego no hay forma de transformarlo en un channel de doble via.
	fmt.Printf("Tipo del channel numero antes de la llamada a generarNumeros: %T\n", numero)
	go generarNumeros(numero)
	fmt.Printf("Tipo del channel numero: %T\n", numero) // VER LA TRANSFORMACION EXPLICITAMENTE. INVESTIGAR!!
	// Lo elevamos al cuadrado
	go elevarAlCuadrado(numero, cuadrado)

	// Desde la gorutina main tomamos el valor del canal cuadrado para imprimirlo
	imprimir(cuadrado)
}