package main
/*
Los channels son los mecanismos de comunicación 
Permiten que una gorutina envie valores a otra gorutina
Cuando pasamos un channel por parametro lo estamos pasando POR REFERENCIA
*/

import (
	"fmt"
	"time"
)

func imprimirPing(ch chan string) {
	var contador int
	for {
		// Recibiendo valores a través del canal
		contador++
		fmt.Println(<-ch, " ", contador)
		time.Sleep(time.Second * 1)
	}
}

func enviarPing(ch chan string) {
	for  {
		for _, word := range `Arnaldo Ceballos - ` {
			// Enviando valores a través del canal
			// Cada vez que envia un valor, channel para la gorutina hasta que sea consumido por imprimirPing
			ch <- string(word)
		}
	}
}

/*
// Version original de enviarPing
func enviarPing(ch chan string) {
	for {
		ch <- "ping"
	}
}
*/

func main() {
	// Creamos un canal
	// Un channel es un tipo y a su vez son de un tipo
	// A través de este channel solo se pueden pasar string
	ch := make(chan string)

	// Llamamos las funciones como gorutinas
	// Usamos el channel para pasar información entre las dos gorutinas
	go enviarPing(ch)
	go imprimirPing(ch)

	// Escaneamos la entrada de datos para que no finalice la gorutina "main"
	// Cuando presiono enter finaliza la ejecución //VER PORQUE
	var input string
	fmt.Scanln(&input)
	fmt.Println("Fin...")

}