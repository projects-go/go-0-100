package main

import (
	"fmt"
)

func imprimir() {
	fmt.Println("Hola Arnaldo")
	defer func() {
		cadena := recover() // Con recover() se recupera el error y quiza es posible tratarlo
		fmt.Println(cadena)
	}()
	panic("Error") // Con panic() se envia errores
}

// Panic detiene la ejecucion de un programa
func main() {
	imprimir()
	fmt.Println("Hola main")
}