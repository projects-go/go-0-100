package main

import (
	"fmt"
)

type Persona struct {
	Nombre string
	Apellido string
}

// Tipo anónimo. Ventaja: Estudiante tiene todos los campos y métodos que tenga la estructura Persona
// No me cierra el argumento. Colocar un nombre también supongo que proporciona la misma ventaja, quizá 
// lo que se evita de esta forma es la necesidad de una redirección más por medio del nombre del campo
// Composición de estructuras
type Estudiante struct {
	Persona 
	Carrera string
}

// Cuando un padre tiene un campo denominado igual que en la estructura hija, siempre se llama
// el campo de la estructura padre y para acceder al campo de la estructura hija se debe especificar que
// es de la estructura hija por medio del nombre de la estructura hija
type Profesor struct {
	Estudiante 
	Carrera string
}

func main() {
	fmt.Println("*********** Estudiante ***********")
	alejandro := Estudiante {
		Persona {
			Nombre: "Alejandro",
			Apellido: "Arnaud",
		},
		"Informatica",
	}
	fmt.Println("Alejandro:", alejandro)

	fmt.Println("Nombre:", alejandro.Nombre)
	fmt.Println("Apellido:", alejandro.Apellido)
	fmt.Println("Carrera:", alejandro.Carrera)

	fmt.Println("*********** Profesor ***********")
	pedro := Profesor {
		Estudiante {
			Persona {
				Nombre: "Pedro",
				Apellido: "Almonte",
			},
			"Contabilidad",
		},
		"Informatica",
	}
	fmt.Println("Pedro:", pedro)
	fmt.Println("Nombre:", pedro.Nombre)
	fmt.Println("Apellido:", pedro.Apellido)
	fmt.Println("Carrera Estudiante:", pedro.Estudiante.Carrera)
	fmt.Println("Carrera:", pedro.Carrera)

	fmt.Println("*********** Profesor ***********")
	var jose Profesor
	jose.Nombre = "Jose"
	jose.Apellido = "Contreras"
	jose.Estudiante.Carrera = "Educación"
	jose.Carrera = "Mercadeo"
	fmt.Println("Jose:", jose)
}