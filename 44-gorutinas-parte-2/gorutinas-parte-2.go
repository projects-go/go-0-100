package main

import (
	"fmt"
	"time"
)

func animacion(retraso time.Duration) {
	for	{ // Bucle infinito
		for _, r := range `\|/-` {
			fmt.Printf("\r%c", r)
			time.Sleep(retraso)
		}
	}
}

func fib(x int) int {
	if x < 2 {
		return x	
	}
	return fib(x-1) + fib(x-2)
}

func main() { // main es llamado como una gorutina!!!
	go animacion(100 * time.Millisecond) // Va a estar ejecutando la animacion hasta que la funcion fib devuelva el resultado
	// NO ENTIENDO PORQUE SE DETIENE LA ANIMACION. VER!!!!
	const n = 45 // No hay necesidad de que sea una variable. No va a cambiar!
	resultado := fib(n) // Aqui el programa se queda esperando el resultado
	fmt.Printf("\rFibonacci(%d) = %d\n", n, resultado)
}