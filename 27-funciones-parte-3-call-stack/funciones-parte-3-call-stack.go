package main

import (
	"fmt"
)

func f1() {
	fmt.Println("Entrando en f1")
	f2()
	fmt.Println("Saliendo de f1")
}

func f2() {
	fmt.Println("Entrando en f2")
	f3()
	fmt.Println("Saliendo de f2")
}

func f3() {
	fmt.Println("Entrando en f3")
	fmt.Println("Saliendo de f3")
}

func main() {
	fmt.Println("Entrando en main")
	f1()
	fmt.Println("Saliendo de main")
}

