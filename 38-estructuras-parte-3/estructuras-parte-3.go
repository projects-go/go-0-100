package main

import (
	"fmt"
	"math"
)

type Rectangulo struct {
	ancho, alto float64
}

// Circulo: es de tipo struct pero con nuestra configuración
type Circulo struct {
	radio float64
}

// r Rectangulo: r es un recibidor
func (r Rectangulo) area() float64 {
	return r.ancho * r.alto
}

func (c Circulo) area() float64 {
	return c.radio * c.radio * math.Pi
}

func (r Rectangulo) inc(i float64) Rectangulo {
	return Rectangulo {
		r.ancho * i,
		r.alto * i,
	}
}

func (r *Rectangulo) inc2(i float64) {
		r.ancho *= i
		r.alto *= i
}

func main() {
	fmt.Println("************ Rectangulos ************")
	r1 := Rectangulo{12, 2}
	r2 := Rectangulo{9, 4}
	fmt.Println("Area de r1 es: ", r1.area())
	fmt.Println("Area de r2 es: ", r2.area())

	fmt.Println("************ Circulos ************")
	c1 := Circulo{10}
	c2 := Circulo{25}
	fmt.Println("Area de c1 es: ", c1.area())
	fmt.Println("Area de c2 es: ", c2.area())

	fmt.Println("************ Retorno de rectangulo incrementado ************")
	fmt.Println("r1 es: ", r1)	
	r1 = r1.inc(10) // No es lo ideal que retorne la estructura, sino que el incremento se aplique sobre la estructura que lo invoca (el rectangulo)
	fmt.Println("r1 incrementado es: ", r1)	

	fmt.Println("************ Rectangulo incrementado ************")
	fmt.Println("r2 es: ", r2)	
	r2.inc2(10) // Esto es lo ideal!!
	fmt.Println("r2 incrementado es: ", r2)	
}