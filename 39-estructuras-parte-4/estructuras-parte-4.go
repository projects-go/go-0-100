package main

import (
	"fmt"
)

type Punto struct {
	x, y int
}

type Punto3D struct {
	x, y, z int
	// Punto3D // No puede contener como campo una estructura del mismo tipo (invalid recursive type Punto3D)
	*Punto3D
}

// Se pueden declarar estructuras sin campos
// Utilidad: porque se le van a agregar métodos para ser utilizados desde esta estructura
// Se le llama estructura de tamaño 0
type OpPunto struct {}

func main() {
	fmt.Println("************ Punto ************")
	p := Punto {}
	fmt.Println("p", p)

	fmt.Println("************ Punto3D ************")
	p2 := Punto3D {
		5, 
		6, 
		4, 
		&Punto3D {
			6,
			4,
			6,
			nil,
		},
	}
	fmt.Println("p2", p2)

	fmt.Println("************ Comparar estructuras ************")
	a := Punto {5, 6}
	b := Punto {7, 4}
	fmt.Println("a == b", a == b)

	c := Punto {7, 4}
	fmt.Println("b == c", b == c)

	fmt.Println("************ Estructuras utilizadas como índices en los mapas ************")
	fmt.Println("---> Las estructuras como son comparables se pueden utilizar como indices en los maps")
	figuras := make(map[Punto]string)
	figuras[a] = "Hola Mundo"
	fmt.Println("figuras[a]:", figuras[a])
}