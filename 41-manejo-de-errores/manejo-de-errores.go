package main

/*
Go no tiene manejo de excepciones.
En go se define:
type error interface {
	Error() string
}
*/
import (
	"fmt"
	"errors"
)

func baneado(usuario string) (err error) {
	ban := false
	switch usuario {
		case "miguel":
			ban = true
		case "carlos":
			ban = false
		case "juan":
			return fmt.Errorf("El usuario no es valido")
		case "pedro":
			return fmt.Errorf("Usuario en proceso de registro")
		default:
			return fmt.Errorf("Algo paso xd...")
	}
	if !ban {
		fmt.Printf("El usuario %s no esta baneado\n", usuario)
	} else {
		fmt.Printf("El usuario %s esta baneado\n", usuario)
	}
	return nil
}

// Algunos plugins piden que sean comentados los elementos que pueden ser exportados
// ...: sirven para decir que se que tengo que comentar pero de esta forma no nos genera un mensaje de alerta

// Todos los errores que el paquete puede dar estan juntos
// Ejemplo: Sección Variables de https://golang.org/pkg/bufio/ 
var (
	// Algunos plugins piden que sean comentados ...
	ErrorUsuarioNoValido = errors.New("El usuario no es valido")
	// Algunos plugins piden que sean comentados
	ErrorUsuarioEnProceso = errors.New("Usuario en proceso de registro")
	// Algunos plugins piden que sean comentados
	ErrorPorDefecto = errors.New("Algo paso xd...")
)

func baneado2(usuario string) (err error) {
	ban := false
	switch usuario {
		case "miguel":
			ban = true
		case "carlos":
			ban = false
		case "juan":
			return ErrorUsuarioNoValido
		case "pedro":
			return ErrorUsuarioEnProceso
		default:
			return ErrorPorDefecto
	}
	if !ban {
		fmt.Printf("El usuario %s no esta baneado\n", usuario)
	} else {
		fmt.Printf("El usuario %s esta baneado\n", usuario)
	}
	return nil
}

func checkError(err error) {
	if err != nil {
		fmt.Println("Error:", err)
	}
}

/*
//Ver en: https://golang.org/doc/effective_go.html#errors

By convention, errors have type error, a simple built-in interface.

type error interface {
    Error() string
}
*/

// PathError records an error and the operation and
// file path that caused it.
type PathError struct {
    Op string    // "open", "unlink", etc.
    Path string  // The associated file.
    Err error    // Returned by the system call.
}

func (e PathError) Error() string {
    return e.Op + " - " + e.Path + ": " + e.Err.Error()
}

// Lo inventé yo, pero dudo que este correcto el tratamiento
func baneado3(usuario string) (err error) {
	ban := false
	switch usuario {
		case "miguel":
			ban = true
		case "carlos":
			ban = false
		case "juan":
			novalido := PathError{"No Valido", "usuario", errors.New("El usuario no es valido")}
			return novalido
		case "pedro":
			enproceso := PathError{"En Proceso", "usuario", errors.New("Usuario en proceso de registro")}
			return enproceso
		default:
			pordefecto := PathError{"Por Defecto", "usuario", errors.New("Algo paso xd...")}
			return pordefecto
	}
	if !ban {
		fmt.Printf("El usuario %s no esta baneado\n", usuario)
	} else {
		fmt.Printf("El usuario %s esta baneado\n", usuario)
	}
	return nil
}

func main() {
	fmt.Println("************ Usuario Miguel ************")
	err := baneado("miguel")
	// Inmediatamente se maneja el error!!! Esto es una convención
	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("************ Usuario Juan ************")
	err = baneado("juan")
	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("************ Usuario Carlos ************")
	err = baneado("carlos")
	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("************ Usuario Pedro ************")
	err = baneado("pedro")
	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("************ Usuario Pololo ************")
	err = baneado("pololo")
	if err != nil {
		fmt.Println("Error:", err)
	}

	fmt.Println("************ Para evitar repetir código uso manejador de errores ************")
	err = baneado("miguel")
	checkError(err)
	err = baneado("juan")
	checkError(err)
	err = baneado("carlos")
	checkError(err)
	err = baneado("pedro")
	checkError(err)
	err = baneado("pololo")
	checkError(err)

	fmt.Println("************ Variables para manejo de errores ************")
	err = baneado2("miguel")
	checkError(err)
	err = baneado2("juan")
	checkError(err)
	err = baneado2("carlos")
	checkError(err)
	err = baneado2("pedro")
	checkError(err)
	err = baneado2("pololo")
	checkError(err)

	fmt.Println("************ Con mi versión de manejo de errores con struct (ver si es correcto) ************")
	err = baneado3("miguel")
	checkError(err)
	err = baneado3("juan")
	checkError(err)
	err = baneado3("carlos")
	checkError(err)
	err = baneado3("pedro")
	checkError(err)
	err = baneado3("pololo")
	checkError(err)
}