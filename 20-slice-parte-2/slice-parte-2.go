package main

import "fmt"
import "gitlab.com/arnaldoceballos/utils"

func main() {
	// Definamos un array con 10 elementos de tipo int
	var ar = [6]int{0, 1, 2, 3, 4, 5}
	utils.Map(utils.PrintlnInfo, ar)
	
	// Definimos dos slice de tipo []int
	fmt.Println("--> Definimos dos slice de tipo []int")
	var a, b []int
	utils.Map(utils.PrintlnInfo, a)
	utils.Map(utils.PrintlnInfo, b)

	// a apunta del 3er al 5to elemento del array ar
	fmt.Println("--> a apunta del 3er al 5to elemento del array ar")
	a = ar[2:5]
	utils.Map(utils.PrintlnInfo, a)

	// b apunta del 4to al 5to elemento del array ar
	fmt.Println("--> b apunta del 4to al 5to elemento del array ar")
	b = ar[3:5]
	utils.Map(utils.PrintlnInfo, b)

	fmt.Println("--> Cambio del elemento en la posición 0 del slice b")
	b[0] = 25
	utils.Map(utils.PrintlnInfo, a)
	utils.Map(utils.PrintlnInfo, b)
	utils.Map(utils.PrintlnInfo, ar)

	//Map(printlnInfo, "Hola arnaldo")
	var aa int32 = 99
	utils.Map(utils.PrintlnInfo, aa)
}