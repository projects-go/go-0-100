package main

import (
	"fmt"
)

type Persona struct {
	nombre string
	email string
	edad int
}

func (p Persona) Nombre() string {
	return p.nombre
}

func (p Persona) Email() string {
	return p.email
}

type Moderador struct {
	Persona
	Foro string
}

func (m Moderador) AbrirForo() {
	fmt.Println("Abrir un Foro")
}

func (m Moderador) CerrarForo() {
	fmt.Println("Cerrar un Foro")
}

type Administrador struct {
	Persona
	Seccion string
}

func (a Administrador) CrearForo() {
	fmt.Println("Crear un Foro")
}

func (a Administrador) ElminarForo() {
	fmt.Println("Eliminar un Foro")
}

func Presentarse(p Persona) {
	fmt.Println("Nombre:", p.Nombre())
	fmt.Println("Email:", p.Email())
}

func PresentarseM(m Moderador) {
	fmt.Println("Nombre:", m.Nombre())
	fmt.Println("Email:", m.Email())
}

func PresentarseA(a Administrador) {
	fmt.Println("Nombre:", a.Nombre())
	fmt.Println("Email:", a.Email())
}

// Las interfaces son una herramienta de programación que nos permiten separar a la definición de los objetos de su implementación
// Se utilizan mucho en go debido a que no hay clases, entonces se trabaja con composición
// No hay que especificar que Persona implementa la interface, es redundante. Si los métodos que se delaran en una interface ya los tiene la estructura, entonces la implementa
type Usuario interface {
	Nombre() string
	Email() string
}

func PresentarseI(p Usuario) { //Usuario es una interface --> interface es un tipo
	fmt.Println("Nombre:", p.Nombre())
	fmt.Println("Email:", p.Email())
}

func main() {
	
	fmt.Println("************** Persona **************")
	alejandro := Persona {"Alejandro", "alejandro@gmail.com", 29}
	Presentarse(alejandro)

	fmt.Println("************** Moderador **************")
	juan := Moderador { Persona {"Juan", "juan@gmail.com", 46}, "Juegos"}
	PresentarseM(juan)

	fmt.Println("************** Administrador **************")
	pedro := Administrador { Persona {"Pedro", "pedro@gmail.com", 25}, "PC"}
	PresentarseA(pedro)

	fmt.Println("************** Presentarse usando la Interface **************")
	PresentarseI(alejandro)
	PresentarseI(juan)	
	PresentarseI(pedro)

	fmt.Println("************** Una Interface es un tipo **************")
	var i Usuario
	fmt.Println("---> A la var i de tipo Usuario (interface) le asigno alejandro (Persona)")
	i = alejandro // alejendro que es de tipo Persona, aplica a la interface Usuario
	fmt.Println("i:", i)
	fmt.Println("i.Nombre():", i.Nombre())
	fmt.Println("i.Email():", i.Email())

	fmt.Println("---> A la var i de tipo Usuario (interface) le asigno juan (Moderador)")
	i = juan
	fmt.Println("i:", i)
	fmt.Println("i.Nombre():", i.Nombre())
	fmt.Println("i.Email():", i.Email())
	// fmt.Println("i.CerrarForo():", i.CerrarForo()) //i.CerrarForo undefined (type Usuario has no field or method CerrarForo)
}