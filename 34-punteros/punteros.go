package main

import (
	"fmt"
)

func incrementar(numero int) {
	numero++
	fmt.Println("Valor de numero dentro de la funcion incrementar:", numero)
}

func incrementar2(numero *int) {
	*numero++
	fmt.Println("Valor de numero dentro de la funcion incrementar:", *numero)
}

func main() {
	fmt.Println("************ Se asgina a la variable a 25 ************")
	a := 25
	fmt.Printf("Tipo de a: %T\n", a)
	fmt.Println("Valor de a:", a)
	fmt.Println("Dirección de a:", &a)

	fmt.Println("************ La variable b apunta a la variable a ************")
	b := &a
	fmt.Printf("Tipo de b: %T\n", b)
	fmt.Println("Valor de b:", b) // Dirección de a
	fmt.Println("Valor en la dirección almancenada por b:", *b) // Valor en la dirección almacenada por b

	fmt.Println("************ Se cambia el valor en la dirección apuntada por b por 32 ************")
	*b = 32
	fmt.Println("Valor de a:", a)
	fmt.Println("Valor en la dirección almancenada por b:", *b) // Valor en la dirección almacenada por b

	fmt.Println("************ Se incrementa el valor de la variable a ************")
	a++
	fmt.Println("Valor de a:", a)
	fmt.Println("Valor en la dirección almancenada por b:", *b) // Valor en la dirección almacenada por b

	fmt.Println("************ Zero value de los punteros es nil ************")
	if b != nil {
		fmt.Println("b es diferente de nil")
	}

	fmt.Println("************ Punteros comparables ************")
	c := &a
	if b == c {
		fmt.Println("b y c son iguales")
	}

	fmt.Println("************ Uso de new(). Alternativa (antes &) para obtener la dirección de una variable ************")
	d := new(int) // new devuelve un puntero, en este caso a enteros. d tiene una dirección
	fmt.Printf("Tipo de d: %T\n", d)
	fmt.Println("Valor de d:", d)
	fmt.Println("Dirección de d:", &d)
	fmt.Println("Valor en la dirección almancenada por d:", *d) // Valor en la dirección almacenada por d

	fmt.Println("************ A d se le asgina b ************")
	d = b
	fmt.Println("Valor de d:", *d)
	fmt.Println("Dirección de d:", d)

	fmt.Println("************ Muestra de valores ************")
	fmt.Println("Valor de a:", a)
	fmt.Println("Valor de b:", *b)
	fmt.Println("Valor de c:", *c)
	fmt.Println("Valor de d:", *d)

	fmt.Println("************ Llamada a función con parametros por valor ************")
	numero := 2
	fmt.Println("Numero antes de incrementar:", numero)
	incrementar(numero)
	fmt.Println("Numero después de incrementar:", numero)

	fmt.Println("************ Llamada a función con parámetros por referencia ************")
	fmt.Println("Numero antes de incrementar:", numero)
	incrementar2(&numero)
	fmt.Println("Numero después de incrementar:", numero)

}