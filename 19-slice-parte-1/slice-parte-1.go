package main

import "fmt"

type Mapable interface {}

func printlnInfo(value Mapable) {
	switch value.(type) {
		case string:
			fmt.Println("STRING.", value)
		case int:
			fmt.Println("INT.", value)
		case []int:
			fmt.Println("SLICE OF INT. LEN:", len(value.([]int)), "CAP:", cap(value.([]int)))
			
	}
}

func printfInfo(value Mapable) {
	switch value.(type) {
		case string:
			fmt.Printf("STRING. %s\n", value)
		case int:
			fmt.Printf("INT. %d\n", value)
		case []int:
			fmt.Printf("SLICE OF INT. %v. LEN: %d. CAP: %d\n", value, len(value.([]int)), cap(value.([]int)))
	}
}

// This function maps the every element for all []types of array.
func Map(mapper func(Mapable), list ...Mapable) {
    for _, value := range list {
        mapper(value)
    }
}

func main() {
	// Declarar slices
	var j []int
	Map(printlnInfo, j)

	// Declarar slices 2
	x := []int{1, 2, 3}
	Map(printlnInfo, x)

	// Declarar slice usando make, indicando la longitud
	y := make([]int, 5)
	Map(printlnInfo, y)

	// Declarar slice usando make, indicando la longitud y la capacidad
	k := make([]int, 5, 10)
	Map(printlnInfo, k)

	// Definamos un array con 10 elementos de tipo int
	var ar = [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	Map(printlnInfo, ar)
	Map(printfInfo, ar)

	// Solo para probar la función Map
	Map(printlnInfo, "hola arnaldo")
	Map(printfInfo, "hola arnaldo")
	Map(printlnInfo, 3)
	Map(printfInfo, 3)
}