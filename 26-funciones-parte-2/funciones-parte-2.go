package main

import (
	"fmt"
)

func resta(n1 int, n2 int) (r int) {
	r = n1 - n2
	return 
}

func main() {
	fmt.Println(resta(25, 66))
	// Firma de función resta
	fmt.Printf("%T", resta)
}

