package main

import (
	"fmt"
)

func imprimirNombre(nombre string) {
	fmt.Println("Fuera del main")
	fmt.Println("El nombre es:", nombre)
}

func suma(n1 int, n2 int) int {
	return n1 + n2
}

func main() {
	imprimirNombre("Jose")
	fmt.Println("Dentro del main")

	fmt.Println(suma(25, 66))
}

