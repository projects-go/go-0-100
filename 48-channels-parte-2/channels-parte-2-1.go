package main

import (
	"fmt"
	"time"
)

// En este ejemplo se sincronizan 3 gorutinas: las dos anónimas y la gorutina main

func main() {
	numero := make(chan int)
	cuadrado := make(chan int)

	// Generamos los números
	go func() {
		for x := 0; ; x++ {
			numero <- x
		}
	}()

	// Lo enviamos al cuadrado
	go func() {
		for {
			x := <- numero
			cuadrado <- x * x	
		}
	}()

	// Desde la gorutina main tomamos el valor del canal cuadrado para imprimirlo
	for {
		fmt.Println(<-cuadrado)
		time.Sleep(1 * time.Second)
	}
}